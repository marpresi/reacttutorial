import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}

function  Ingreso(){
  return (<div>
              <Welcome name="Alberto"></Welcome>
              <Welcome name="Federico"></Welcome>
              <Welcome name="Eduardo"></Welcome>
          </div>);
}


function formatName(user){
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const link = <a href="https://www.baggio.com.ar">Ir a Link</a>;
const img = <img src="https://www.baggio.com.ar/es/images/logo.png"></img>;

//const injection = response.potentiallyMaliciousInput;

const element = (
  <div>
    <h1>
      Hello, {formatName(user)}!
    </h1>
    <article>
      <header>
        <h3>Esta es una prueba de ReactJS</h3>
      </header>
      <section>
        {link}
      </section>
      <section>
        {img}
      </section>
      <section>
        <h3># Renderizado de un componente</h3>
        <Welcome name="Martin"></Welcome>
        <Welcome name="Juan"></Welcome>
        <Ingreso></Ingreso>
      </section>
    </article>

  </div>
)

function tick() {
  const element = (
    <div>
      <h1>Hello, world!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  ReactDOM.render(element, document.getElementById('root'));
}

//setInterval(tick, 1000);

ReactDOM.render(

  element


  /*<React.StrictMode>
    <App />
  </React.StrictMode>*/,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
